import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import SubscriptionPage from '../views/SubscriptionPage.vue'
import EditCreateSubscription from '../views/EditCreateSubscription.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/subscriptionPage',
    name:'subscription',
    component: SubscriptionPage,
  },
  {
    path: '/editcreatePage/:subscriptionId?',
    name:'editCreate',
    component: EditCreateSubscription,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
