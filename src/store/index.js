import { createStore } from 'vuex'

function checkSubscription(subs, filter) {
  for (const key in filter){
    if (filter[key] && filter[key]!== subs[key])
    return false
  }
  return true

}
//18. База даних поштарки. База даних абонементів: номер будинку, кількість газет, що
//виписуються, перелік назв газет, що виписуються. Організувати вибір за довільним запитом.
export default createStore({
  //Розділ в якому вписуються дані котрі повинні бути реактивними
  state: {
    subscriptionsList:[
      {
        id:1,
        houseNumber: 101,
        newspaperQuantity: 2,
        newspaperNames: ["Українська Правда", "Нове життя"],
      },
      {
        id:2,
        houseNumber: 202,
        newspaperQuantity: 1,
        newspaperNames: "Порадниця",
      },
      { 
        id:3,
        houseNumber: 303,
        newspaperQuantity: 3,
        newspaperNames: ["Українська Правда", "Пізнайко", "Порадниця"],
      },
      {
        id:4,
        houseNumber: 404,
        newspaperQuantity: 2,
        newspaperNames: ["Пізнайко", "Порадниця"],
      },
    ],
    filterObj : {},
  },
  //Розділ де описуємо гетери щоб отримувати дані з стейта можливо виконуючи форматування
  getters: {
    getSubscriptions:({subscriptionsList, filterObj})=>subscriptionsList.filter(subs=> checkSubscription(subs,filterObj)),
    getSubscriptionById : (state) => (subscriptionId) => state.subscriptionsList.find(subs=>subs.id== subscriptionId)
  },
  //Мутації методи які мають право змінювати стейт
  mutations: {
    removeSubscription(state,subscriptionId) {
      state.subscriptionsList = state.subscriptionsList.filter((subscriptionsList)=>subscriptionsList.id !==subscriptionId)
    },
    addSubscription(state,subscription) {
      state.subscriptionsList.push(subscription )
    },
    updateSubscription(state,subscription){
      const subscriptionIndex = state.subscriptionsList.findIndex(subs=>subs.id== subscription.id)
      state.subscriptionsList[subscriptionIndex] = subscription
    },

    updateFilter(state, filter){
      state.filterObj = filter
    }
  },
  //Методи які ініціюють зміни викликають мутації
  actions: {
    deleteSubscription({commit},idToRemove){
      commit('removeSubscription',idToRemove)
    },
    onAddSubscription({commit},subsObj){
      commit('addSubscription',{
        id: new Date().getTime(),
        ...subsObj,
      })
      
    },
    onUpdateSubscription({commit},subscription){
      commit('updateSubscription',subscription)
    },

    onUpdateFilter({commit}, filter){
      commit('updateFilter', filter)
    }
  },
  // 
  modules: {
  }
})
